'use strict';

let camera, renderer, scene;
let sun, pointLights = [0,0,0,0];
let spotLights = [];
let time_stamp;
let plane;
const colorBody = 0x797B84,
		colorWings = 0xADA296,
		colorCockpit = 0x46FFFF,
		colorStabilizers = 0xaaaaaa;

//const colorSpotlight = [0xFFFFFF,0x0000FF,0xFF0000,0x00FF00];
const colorSpotlight = [0xf0c800, 0xf0c800, 0xf0c800, 0xf0c800];

let rotateY = 0, rotateX = 0;

function onResize() {
	'use strict';
	/*atualiza o renderer para o tamanho novo da janela*/
	renderer.setSize(window.innerWidth, window.innerHeight);
	camera.aspect = renderer.getSize().width / renderer.getSize().height;
	camera.updateProjectionMatrix();
}

//Objecto spotLight
// x, y, z : posicoes
// color : cor da luz
function SpotlightObject (x, y, z, color) {
	this.object = new THREE.Object3D();
	this.object.position.set(x, y, z);
	this.object.lookAt(plane.object.position);
	this.materialBasic = [new THREE.MeshBasicMaterial( {color: 0x4B4237, wireframe: false} ), new THREE.MeshBasicMaterial( {color: 0xFFFF00, wireframe: false} )];
	this.materialPhong = [new THREE.MeshPhongMaterial({color:0x4B4237, specular: 0xffffff, shininess: 100, opacity: 0.9}), new THREE.MeshPhongMaterial({color:0xFFFF00, specular: 0xffffff, shininess: 100, opacity:0.1})]
	this.materialLambert = [new THREE.MeshLambertMaterial({color: 0x4B4237}), new THREE.MeshLambertMaterial({color: 0xFFFF00, opacity:0.1})];
	this.phong = true;
	this.addSupport = function(x, y, z) {
		const material =  this.materialPhong[0];
		const materialLamp = this.materialPhong[1];

		const cone = new THREE.ConeGeometry( 3, 5, 32 );
		const meshCone = new THREE.Mesh(cone, material);

		const lamp = new THREE.SphereGeometry (1.5, 21, 21);
		const meshLamp = new THREE.Mesh(lamp, materialLamp);
		meshLamp.position.set(0, -2, 0)

		meshCone.add(meshLamp);
		meshCone.rotateX(-Math.PI/2);
		this.object.add(meshCone);
		this.lampObject = meshLamp;
	}

	this.addSpotlight = function(x, y, z, color) {
		this.spotLight = new THREE.SpotLight( color , 0.5);
		this.spotLight.position.set( x, y, z );
		this.lampObject.add( this.spotLight );
	}

	this.switchLight = function (i) {
		this.spotLight.intensity = this.spotLight.intensity ? 0 : 0.5;
		const res = (this.object.children[0].children[0].material.color.getHex() == 0xFFFF00) ? 0x252A2F : 0xFFFF00;
		this.object.children[0].children[0].material.color.setHex(res);
	}

	this.changeLightCal = function () {
		
		if (this.object.children[0].material.type != 'MeshBasicMaterial') {
			this.object.children[0].material = this.materialBasic[0];
			this.object.children[0].children[0].material = this.materialBasic[1];
		}
		else {
			if (this.phong) {
				this.object.children[0].material = this.materialPhong[0];
				this.object.children[0].children[0].material = this.materialPhong[1];
			}
			else {
				this.object.children[0].material = this.materialLambert[0];
				this.object.children[0].children[0].material = this.materialLambert[1];
			}
		}
	}

	this.changeRef = function () {
		if (this.object.children[0].material.type == 'MeshBasicMaterial') this.phong = !this.phong;

		else {
			if (this.phong) {
				console.log('trocou')
				this.object.children[0].material = this.materialLambert[0];
				this.object.children[0].children[0].material = this.materialLambert[1];
				this.phong = false;
			}
			else {
				this.phong = true;
				this.object.children[0].material = this.materialPhong[0];
				this.object.children[0].children[0].material = this.materialPhong[1];
			}		
		}
	}

	this.addSupport(0,0,0);
	this.addSpotlight(0, 0, 0, color);
	scene.add(this.object);
}

/* Objecto Plane
 x, y, z : posicao */
function Plane(x, y, z) {
	this.object = new THREE.Object3D();
	this.object.position.set(x, y, z);

	this.phong = true;

	this.materialBasic = [new THREE.MeshBasicMaterial({color:colorBody}),
												new THREE.MeshBasicMaterial({color:colorWings}),
												new THREE.MeshBasicMaterial({color:colorCockpit}),
												new THREE.MeshBasicMaterial({color:colorStabilizers})]

	this.materialPhong = [new THREE.MeshPhongMaterial({color:colorBody, specular: 0xffffff, shininess: 100}),
												new THREE.MeshPhongMaterial({color:colorWings, specular: 0x873838, shininess: 40}),
												new THREE.MeshPhongMaterial({color:colorCockpit, specular: 0xffffff, shininess: 0, transparent: true, opacity: 0.6}),
												new THREE.MeshPhongMaterial({color:colorStabilizers, specular: 0x73c32d, shininess: 30 })]

	this.materialLambert = [new THREE.MeshLambertMaterial({color: colorBody}),
													 new THREE.MeshLambertMaterial({color: colorWings}),
													 new THREE.MeshLambertMaterial({color: colorCockpit}),
													 new THREE.MeshLambertMaterial({color: colorStabilizers})]




	this.addBody = function () {
		let geometry = new THREE.Geometry()
		const materialBody = this.materialPhong[0];
		composePlaneSquare(geometry, new THREE.Vector3(5, -2, 15), new THREE.Vector3(5, -2, -5), new THREE.Vector3(-5, -2, -5), new THREE.Vector3(-5, -2, 15), 10, 20);
		composePlaneSquare(geometry, new THREE.Vector3(-5, -2, 15), new THREE.Vector3(-5, 3, -5),  new THREE.Vector3(5, 3, -5),  new THREE.Vector3(5, -2, 15), 10, 20 );
		composePlaneSquare(geometry, new THREE.Vector3(5, -2, -5), new THREE.Vector3(5, 3, -5),  new THREE.Vector3(-5, 3, -5),  new THREE.Vector3(-5, -2, -5), 10, 20 );
		composePlaneTriangle(geometry, new THREE.Vector3(5, -2, 15), new THREE.Vector3(5, 3, -5), new THREE.Vector3(5, -2, -5), 6);
		composePlaneTriangle(geometry, new THREE.Vector3(-5, -2, 15), new THREE.Vector3(-5, -2, -5), new THREE.Vector3(-5, 3, -5), 6);
		let mesh = new THREE.Mesh(geometry, materialBody);
		this.object.add(mesh);
	}

	this.addWings = function () {
		let geometry = new THREE.Geometry()
		const materialWings = this.materialPhong[1];
		composePlaneTriangle(geometry, new THREE.Vector3(5, 0.5, 2 ), new THREE.Vector3(18, 0, 3), new THREE.Vector3(5, -0.5, 6), 6)
		composePlaneTriangle(geometry, new THREE.Vector3(5, 0.5, 2), new THREE.Vector3(5, -0.5, 6), new THREE.Vector3(18, 0, 3), 6)

		composePlaneTriangle(geometry, new THREE.Vector3(-5, 0.5, 2 ), new THREE.Vector3(-18, 0, 3), new THREE.Vector3(-5, -0.5, 6), 6)
		composePlaneTriangle(geometry, new THREE.Vector3(-5, 0.5, 2), new THREE.Vector3(-5, -0.5, 6), new THREE.Vector3(-18, 0, 3), 6)

		let mesh = new THREE.Mesh(geometry, materialWings);
		this.object.add(mesh);
	}

	this.addCockpit = function () {
		let geometry = new THREE.Geometry()
		const materialCockpit = this.materialPhong[2];

		composePlaneSquare(geometry, new THREE.Vector3(2,-0.8,9), new THREE.Vector3(2,1.7,9), new THREE.Vector3(2,1.7,0), new THREE.Vector3(2,-0.8,0), 5, 10);
		composePlaneSquare(geometry, new THREE.Vector3(-2, -0.8, 0), new THREE.Vector3(-2, 1.7, 0), new THREE.Vector3(-2, 1.7, 9), new THREE.Vector3(-2, -0.8, 9), 5, 10);
		composePlaneSquare(geometry, new THREE.Vector3(2, 1.7, 9), new THREE.Vector3(-2, 1.7, 9), new THREE.Vector3(-2, 1.7, 0), new THREE.Vector3(2, 1.7, 0), 5, 10);
		composePlaneSquare(geometry, new THREE.Vector3(-2, -0.8, 9), new THREE.Vector3(-2, 1.7, 9), new THREE.Vector3(2, 1.7, 9), new THREE.Vector3(2, -0.8, 9), 5, 10);

		let mesh = new THREE.Mesh(geometry, materialCockpit);
		this.object.add(mesh);
	}

	this.addStabilizers = function() {
		let geometry = new THREE.Geometry()
		composePlaneTriangle(geometry, new THREE.Vector3(5, 0, -5 ), new THREE.Vector3(12, 0, -5), new THREE.Vector3(5, 0, -1), 6)
		composePlaneTriangle(geometry, new THREE.Vector3(5, 0, -1), new THREE.Vector3(12, 0, -5), new THREE.Vector3(5, 0, -5), 6)

		composePlaneTriangle(geometry, new THREE.Vector3(-5, 0, -5), new THREE.Vector3(-12, 0, -5), new THREE.Vector3(-5, 0, -1), 6)
		composePlaneTriangle(geometry, new THREE.Vector3(-5, 0, -1), new THREE.Vector3(-12, 0, -5), new THREE.Vector3(-5, 0, -5), 6)

		composePlaneTriangle(geometry, new THREE.Vector3(0, 1, -5), new THREE.Vector3(0, 1, -1), new THREE.Vector3(0, 7, -5), 6)
		composePlaneTriangle(geometry, new THREE.Vector3(0, 7, -5), new THREE.Vector3(0, 1, -1), new THREE.Vector3(0, 1, -5), 6)

		const materialStabilizers = this.materialPhong[3];
		let mesh = new THREE.Mesh(geometry, materialStabilizers);
		this.object.add(mesh);
	}

	this.changeLightCal = function () {
		let i = 0;

		if (this.object.children[1].material.type != 'MeshBasicMaterial')
			for (i; i < 4; i++)
				this.object.children[i].material = this.materialBasic[i];

		else {
			if (this.phong)
				for (i ; i < 4; i++)
					this.object.children[i].material = this.materialPhong[i];
			else
				for (i ; i < 4; i++)
					this.object.children[i].material = this.materialLambert[i];
		}
	}

	this.changeRef = function () {
		let i = 0;
		if (this.object.children[1].material.type == 'MeshBasicMaterial') this.phong = !this.phong;

		else {
			if (this.phong)
				for (i ; i < 4; i++) {
					this.object.children[i].material = this.materialLambert[i];
					this.phong = false;
				}
			else
				for (i ; i < 4; i++) {
					this.phong = true;
					this.object.children[i].material = this.materialPhong[i];
				}
		}
	}

	this.addBody();
	this.addWings();
	this.addCockpit();
	this.addStabilizers();
	this.object.add(new THREE.AxesHelper(10))
	this.object.scale.set(1.5,1.5,1.5)

	scene.add(this.object);
}


function onKeyUp(e) {
	'use strict';
	console.log(e.keyCode)
	switch(e.keyCode) {
		case 38:
		case 40:
			rotateY = 0;
			break

		case 37:
		case 39:
			rotateX = 0;
			break;
	}
}

function onKeyDown(e) {
	'use strict';
	let i = 0;
	console.log(e.keyCode);
	switch(e.keyCode) {
		case 78:
			sun.intensity = sun.intensity ? 0 : 0.5;
			break;
		case 49:
			spotLights[0].switchLight(0);

			break;
		case 50:
			spotLights[1].switchLight(1);
			break;
		case 51:
			spotLights[2].switchLight(3);
			break;
		case 52:
			spotLights[3].switchLight(4);
			break;
		case 38:
			rotateY = 1;
			break;
		case 40:
			rotateY = -1;
			break;
		case 37:
			rotateX = -1;
			break;
		case 39:
			rotateX = 1;
			break;
		case 76:
			plane.changeLightCal();
			for (i; i < 4; i++) 
				spotLights[i].changeLightCal();
			break;
		case 71:
			plane.changeRef();
			for (i = 0; i < 4; i++) 
				spotLights[i].changeRef();
			break;
	}
}

/* recebe geometria, v1, v2, v3 com regra da mao direita*/
function drawTriangle (geometry, v1, v2, v3) {
	const i = geometry.vertices.length;
	geometry.vertices.push(v3, v2, v1);
	geometry.faces.push(new THREE.Face3(i, i+1, i+2));
	geometry.computeFaceNormals();
}

/* funcao recursiva que separa um triangulo em triangulos mais pequenos
recebe uma geometria, v1, v2, v3 com regra da mao esquerda
numDivisoes de divisoes que vamos impor, condiciona numero de triangulos gerado,
respetivo as linhas
numColunas argumento que e necessario para o composePlaneTriangle que serve para subsequentemente dividir em colunas*/
function composePlaneTriangle(geometry, v1, v2, v3, numDivisoes) {
	if (numDivisoes == 1) {
		drawTriangle(geometry, v1, v2, v3);
		return;
	}
	const a = v3.clone().sub((v3.clone().sub(v1.clone())).multiplyScalar(1/numDivisoes));
	const b = v2.clone().sub((v2.clone().sub(v1.clone())).multiplyScalar(1/numDivisoes));
	const c = v2.clone().sub((v2.clone().sub(v3.clone())).multiplyScalar(1/numDivisoes));
	drawTriangle(geometry, b, v2, c);
	composePlaneSquare(geometry, a, b, c, v3, numDivisoes, 1);
	//chama-se a si propria, com um sub triangulo do triangulo maior
	composePlaneTriangle(geometry, v1, b, a, numDivisoes - 1);

}


/*converte um quadrado em varios
quadrados e esses em dois triangulos cadas
v1 e o vertice superior esquerdo
v2 o vertice superior direito
v3 e o vertice inferior direito
v4 e o vertice inferiro esquerdo
vemos a vertice a partir da face da normal que queremos que os triangulos tenham*/
function composePlaneSquare(geometry, v1,v2,v3,v4, numLinhas, numColunas) {

	/*tamanhoVertical e tamanhoHorizontal o lado esquerdo e de cima do quadrado original, respetivamente*/
	/*nota: como comecamos no vertice v1 o tamanho vertical vai ser negativo*/
	let tamanhoVertical = v4.clone().sub(v1);
	let tamanhoHorizontal = v2.clone().sub(v1);

	/*da-nos o lado esquerdo (tamanhoVertical) e de cima (tamanhoHorizontal) de cada um dos quadrados pequenos, respetivamente*/
	tamanhoHorizontal.divideScalar(numLinhas);
	tamanhoVertical.divideScalar(numColunas);

	let i, j;
	/*este vetor representa os indices de todos os vertices do ponto no array de vertices da geometria*/
	let indexVector = [];

	/*o inicio do novo vertice e uma posicao apos o fim da anterior*/
	const beginning = geometry.vertices.length;

for (i = 0; i < numColunas + 1 ; i++){
		/*comecamos da esquerda para a direita*/
		indexVector[i] = []
		for(j = 0; j < numLinhas + 1; j++){
			/*comecamos de cima para baixo a adicionar os vertices a geometria e a registar o seu indice no array de vertices da geometria na matriz indexVector*/
			geometry.vertices.push(v1.clone().add(tamanhoHorizontal.clone().multiplyScalar(j)).add(tamanhoVertical.clone().multiplyScalar(i)));
			indexVector[i].push(i * (numLinhas + 1) + j + beginning);

		}
	}

	for (i = 0; i < numColunas ; i++)
		for(j = 0; j < numLinhas ; j++){
			/*converte quadrado mais pequeno em dois triangulos (ordem e para garantir que a normal fica igual a normal do quadrado original)*/
			squareToTriangles(geometry, indexVector[i+1][j+1], indexVector[i][j+1], indexVector[i][j], indexVector[i+1][j]);
}

	/*no fim computamos as normais da geometria toda*/
	geometry.computeFaceNormals();




}

/*converte um quadrado definido por 4 vertices em dois triangulos
i1 e o indice do vertice superior esquerdo na geometria
i2 o indice do vertice superior direito na geometria
i3 e o indice do vertice inferior direito na geometria
i4 e o indice do vertice inferiro esquerdo na geometria
vemos a vertice a partir da face da normal que queremos que os triangulos tenham*/
function squareToTriangles(geometry, i1, i2, i3, i4) {
	geometry.faces.push(new THREE.Face3(i1, i2, i3));
	geometry.faces.push(new THREE.Face3(i3, i4, i1));

}

function createCamera() {
	camera = new THREE.PerspectiveCamera(90, window.innerWidth/window.innerHeight, 2, 300);
	camera.position.x = 32;
	camera.position.z = 32;
	camera.position.y = 32;

	camera.lookAt(scene.position);
	camera.updateProjectionMatrix();
}

function createLights() {
	sun = new THREE.DirectionalLight( 0xFFFFFF, 0.5 );
	sun.position.set( 20, 30, 0 );
	scene.add(sun);

 	spotLights[0] = new SpotlightObject(0, 30, 35, colorSpotlight[0]);
	spotLights[1] = new SpotlightObject(35, 30, 0, colorSpotlight[1]);
	spotLights[2] = new SpotlightObject(35, 0, 0, colorSpotlight[2]);
	spotLights[3] = new SpotlightObject(0, 0, 35, colorSpotlight[3]);
}

function createScene() {
	scene = new THREE.Scene();
}

function init() {
	renderer = new THREE.WebGLRenderer({antialias:true});
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);
	time_stamp = Date.now();

	createScene();
	createCamera();

	plane = new Plane(0,0,0);

	createLights();


	//plane.object.rotateOnWorldAxis(new THREE.Vector3(0,1,0), Math.PI/2

	window.addEventListener("resize", onResize);
	window.addEventListener("keydown", onKeyDown);
	window.addEventListener("keyup", onKeyUp);

}

function render() {
	renderer.render(scene, camera);
}

function animate() {
	let time = Date.now()
	let delta_t = time - time_stamp;
	time_stamp = time;
	plane.object.rotateY( (Math.PI / 3000) * delta_t * rotateX)
	plane.object.rotateX( -(Math.PI / 3000) * delta_t  * rotateY )

	render();
	requestAnimationFrame(animate);
}
